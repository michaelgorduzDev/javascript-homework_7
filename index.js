let arr = [];

for (let i = 0; i < 10; i++) {
    arr[i] = (Math.floor((Math.random()) * 101)) - (Math.floor((Math.random()) * 101));
    arr[i + 1] = "Hello";
    arr[i + 2] = "World"
    arr[i + 3] = null;
    arr[i + 4] = true;
}

filterBy = function (array, type) {
    return array.filter(element => typeof element !== type);
}

console.log("Original array");
console.log(arr);
console.log("Filtered array");
console.log(filterBy(arr, "number"));
